document.addEventListener("DOMContentLoaded",()=>{

    /* Funcion del contador */
const actualizarContador=()=>{
/* Obtiene la lista */

let ul = document.getElementById("lista")

/* Obtiene el contador */
let contador = document.getElementById("contador")
contador.innerHTML=ul.children.length

}

/* Click en el check box */
   /*  function cilckCheckbox() {
        console.log("se hizo cilck en el checkbox");
    } */
    
const cilckCheckbox =(evento)=>{
    
   /* Obtiene al elemento que le hicieron click */
let elemento =evento.target;
   console.log(elemento);
  /* Obtiene el li padre */

  let padre = elemento.parentElement
  console.log(padre);

  /* Cambia la clase css del padre li */

  padre.classList.toggle("terminada")
}
/* ELIMINAR: */

/* Clic en el boton elimar tarea */
const clickEliminar =(evento)=>{
/* Obtiene el boton al que se le hizo click */
let boton =evento.target;
/* Obtener li padre */

let padre = boton.parentElement;
/* console.log(padre); */

/* Elimina el elemento */
padre.remove()
/* Actualiza el contador */
actualizarContador()
}


/* Obtiene el boton para agregar tarea */
 
let boton =document.getElementById("boton")

/* Clic en el boton agregar tarea */

boton.addEventListener("click",() =>{

/* Obtiene el input */

let input =document.getElementById("tarea")
/* Obtiene el texto digitado */

let texto =input.value
/* console.log(texto); */
/* Crear el elemento li */

let li =document.createElement("li")
/* Crea el elemento input de checkbox */
let checkbox =document.createElement("input")
checkbox.setAttribute("type","checkbox")
checkbox.addEventListener("click",cilckCheckbox)
/* Crea el elemento de tipo texto */

let elementoTexto =document.createTextNode(texto)/* Se necesita pasar el texto a un nodo */
/* Crea el boton para borrar */
let botonEliminar =document.createElement("button")
botonEliminar.innerText="x"
botonEliminar.addEventListener("click",clickEliminar)
/* Agrega los elemntos al li */

li.appendChild(checkbox)
li.appendChild(elementoTexto)
li.appendChild(botonEliminar)
/* console.log(li); */

/* Obtiene la lista ul */
let ul =document.getElementById("lista")
ul.appendChild(li)
/* console.log(ul.children.length); */
/* Actualiza el contador */
actualizarContador()
/* Limpia el input */
input.value=""

})


})